<?php

// require 'animal.php';
// require 'ape.php';
require 'frog.php';

$animal = new Animal("Shaun");

echo 'Name : ' . $animal->name . '<br>';
echo 'Legs : ' . $animal->legs . '<br>';
echo 'Cold Blooded : ' . $animal->cold_blooded . '<br><br>';

$kodok = new Frog("Buduk");
echo 'Name : ' . $kodok->name . '<br>';
echo 'Legs : ' . $kodok->legs . '<br>';
echo 'Cold Blooded : ' . $kodok->cold_blooded . '<br>';
echo 'Jump : ';
$kodok->jump();
echo '<br><br>';

$sungokong = new Ape("Kera Sakti");

echo 'Name : ' . $sungokong->name . '<br>';
echo 'Legs : ' . $sungokong->legs . '<br>';
echo 'Cold Blooded : ' . $sungokong->cold_blooded . '<br>';
echo 'Yell : ';
$sungokong->yell();
